﻿<?php

	include 'db.php';
	
	$zustand = "in Bearbeitung";
	
	date_default_timezone_set("Europe/Berlin");
	$timestamp = date("Y.m.d");
	
	$user = "1";
	

	$statement = $db->prepare("INSERT INTO tbl_umfrage (uid, umfrage, zustand, erstellungsdatum, startdatum, enddatum, userid) VALUES (null,:umfrage,:zustand,:timestamp,:sd,:ed,:user)");
	$statement -> bindParam(':umfrage', $_POST['name'], PDO::PARAM_STR); 
	$statement -> bindParam(':zustand', $zustand, PDO::PARAM_STR);
	$statement -> bindParam(':timestamp', $timestamp, PDO::PARAM_STR);
	$statement -> bindParam(':sd', $_POST['s_d'], PDO::PARAM_STR);
	$statement -> bindParam(':ed', $_POST['e_d'], PDO::PARAM_STR);
	$statement -> bindParam(':user', $user, PDO::PARAM_INT);
	$statement->execute();
	
	echo $_POST['e_d'];
	?>