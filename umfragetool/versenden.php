﻿<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Die 3 Meta-Tags oben *m??n* zuerst im head stehen; jeglicher sonstiger head-Inhalt muss *nach* diesen Tags kommen -->
    <title>Bootstrap-Basis-Vorlage</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Unterst??ng f??edia Queries und HTML5-Elemente in IE8 ?? HTML5 shim und Respond.js -->
    <!-- ACHTUNG: Respond.js funktioniert nicht, wenn du die Seite ?? file:// aufrufst -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">Umfrage Tool</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="umfrage.php">Umfrage</a></li>
					<li><a href="email.php">E-mail Gruppen</a></li>
					<li><a href="versenden.php">Versenden</a></li>
				</ul>
			</div>
		</nav>
		<form class="container">
			
			<select id="egruppe" name="egruppe" class="selectpicker">
				<option>Gruppe1</option>
				<option>Gruppe2</option>
				<option>Gruppe3</option>
			</select>
			
  
			
			<select id="umfrage" name="umfrage" class="selectpicker">
				<option>Umfrage1</option>
				<option>Umfrage2</option>
				<option>Umfrage3</option>
			</select>
			
			
			
			<button class="btn btn-primary btn-sm">versenden</button>
		</form>
		<div class="container">
			<table class="table table-hover">
					<thead>
						<tr>
							<th>Datum</th>
							<th>Umfrage</th>
							<th>Gruppe</th>
							<th>Ergebniss</th>
						</tr>
					</thead>

					<tr>
						<td>23.9.2016</td>
						<td>Umfrage1</td>
						<td>Gruppe4</td>
						<td><button class="btn btn-primary btn-default">Ergebnisse</button></td>
					</tr>
					<tr>
						<td>5.7.2016</td>
						<td>Umfrage2</td>
						<td>Gruppe1</td>
						<td><button class="btn btn-primary btn-default">Ergebnisse</button></td>
					</tr>
				<tr>
					<td>5.7.2016</td>
					<td>Umfrage3</td>
					<td>Guppe2</td>
					<td><button class="btn btn-primary btn-default">Ergebnisse</button></td>
				</tr>
			</table>
		</div>

    <!-- jQuery (wird fr Bootstrap JavaScript-Plugins bentigt) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Binde alle kompilierten Plugins zusammen ein (wie hier unten) oder such dir einzelne Dateien nach Bedarf aus -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>