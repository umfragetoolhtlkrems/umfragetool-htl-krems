<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Die 3 Meta-Tags oben *m�ssen* zuerst im head stehen; jeglicher sonstiger head-Inhalt muss *nach* diesen Tags kommen -->
    <title>Bootstrap-Basis-Vorlage</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Unterst�tzung f�r Media Queries und HTML5-Elemente in IE8 �ber HTML5 shim und Respond.js -->
    <!-- ACHTUNG: Respond.js funktioniert nicht, wenn du die Seite �ber file:// aufrufst -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
	<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script>
	$(document).ready(function(){
	$('#he').hide();
	$('#csv').hide();
	$('#hier_erstellen').click(function(){
	$('#csv').hide();
	$('#he').show();
	});
	$('#csv_imp').click(function(){
	$('#he').hide();
	$('#csv').show();
	});
	});
	
	</script>
  </head>
  

  <body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">Umfrage Tool</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="umfrage.php">Umfrage</a></li>
					<li><a href="email.php">E-mail Gruppen</a></li>
					<li><a href="versenden.php">Versenden</a></li>
				</ul>
			</div>
		</nav>
		<div class="container">
			<ul class="nav nav-tabs">
				<li><a id="hier_erstellen" >direkt erstellen</a></li>
				<li><a id="csv_imp">CSV importieren</a></li>
			</ul>	
			<div id="he">
			<h2>E-Mail Gruppe</h2>
				<div class="row">
					<div class="col-lg-2">
						<label for="ename">Name:</label>
						<input type="text" class="form-control" id="ename">
					</div>
					<div class="col-lg-2">
						<label for="email">E-Mail Adresse</label>
						<input type="text" class="form-control" id="email">
					</div>
					<div class="col-lg-5">
						<label></label>
						<button class="btn btn-default btn-success"><span class="glyphicon glyphicon-plus"></span></button>
					</div>
					<div class="col-lg-12">
						<p>
					</div>
					<div class="col-lg-5">
						<ul class="list-group">
							<li class="list-group-item"><a href="#" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>koppimax@gmx.at</li>
						</ul>
					</div>
				</div>
				<button class="btn btn-primary btn-default">Erstellen</button></td>
			</div>
			<div id="csv">
			<h2>CSV Datei importieren</h2>
				<div class="row">
					<div class="col-lg-8">
						<div class="btn btn-default btn-success"><span class="glyphicon glyphicon-plus"></span>Datei importieren</div>
					</div>
				</div>
			</div>
		</div>


    <!-- jQuery (wird für Bootstrap JavaScript-Plugins ben�tigt) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Binde alle kompilierten Plugins zusammen ein (wie hier unten) oder such dir einzelne Dateien nach Bedarf aus -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>